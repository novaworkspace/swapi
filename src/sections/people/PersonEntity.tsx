import { useRouter } from 'next/router';
import { FC } from 'react';

import { Stack } from '@mui/material';

import { useDisclosure } from '../../hooks/useDisclosure';
import { useGetPersonQuery, useUpdatePersonMutation } from '../../redux/people/people.api';
import { IPeople } from '../../redux/people/people.types';

import { PersonProfile, PersonProfileSkeleton } from './components/PersonProfile';
import { PersonProfileEditModal } from './components/PersonProfileEdit.modal';

type TPersonEntityProps = {
  personId: string;
};

export const PersonEntity: FC<TPersonEntityProps> = ({ personId }) => {
  const modal = useDisclosure();

  const getPerson = useGetPersonQuery(personId, { skip: !personId });
  const [updatePerson] = useUpdatePersonMutation();

  const handleUpdatePerson = (data: IPeople) => {
    // do additional steps here
    updatePerson(data);
  };

  return (
    <Stack spacing={4}>
      {getPerson.isLoading && <PersonProfileSkeleton />}
      {getPerson.isSuccess && <PersonProfile data={getPerson.data} onEdit={modal.onOpen} />}

      {getPerson.data && (
        <PersonProfileEditModal
          data={getPerson.data}
          open={modal.isOpen}
          onClose={modal.onClose}
          onSave={handleUpdatePerson}
        />
      )}
    </Stack>
  );
};

import { FC } from 'react';

import { Box, Stack, Typography } from '@mui/material';

import { IPeople } from '../../../redux/people/people.types';

type TPersonProfileFieldProps = {
  name: keyof IPeople;
  field: IPeople[keyof IPeople];
};

const getFieldValue = (field: IPeople[keyof IPeople]) => {
  if (typeof field === 'string') {
    return field;
  }

  if (Array.isArray(field)) {
    if (field.length === 0) {
      return 'No data';
    }

    return (
      <Stack spacing={1} direction="column">
        {field.map((item, index) => (
          <Typography key={index} variant="body2">
            {getFieldValue(item)}
          </Typography>
        ))}
      </Stack>
    );
  }

  return JSON.stringify(field);
};

export const PersonProfileField: FC<TPersonProfileFieldProps> = ({ name, field }) => (
    <Box gridColumn="span 6">
      <Typography variant="caption" color="text.secondary">
        {name}
      </Typography>
      <Typography variant="body2">{getFieldValue(field)}</Typography>
    </Box>
  );

import React, { FC, useState } from 'react';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Stack,
  TextField,
} from '@mui/material';

import { IPeople } from '../../../redux/people/people.types';

type TPersonProfileProps = {
  open: boolean;
  data: IPeople;
  onSave: (data: IPeople) => void;
  onClose: () => void;
};

export const PersonProfileEditModal: FC<TPersonProfileProps> = ({ open, data, onClose, onSave }) => {
  const [person, setPerson] = useState<IPeople>(data);

  // editing only primitive fields
  const fields: (keyof IPeople)[] = [
    'birthYear',
    'eyeColor',
    'gender',
    'hairColor',
    'height',
    'mass',
    'name',
    'skinColor',
    'created',
    'edited',
    'url',
  ];

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPerson({
      ...person,
      [e.target.name]: e.target.value,
    });
  };

  const handleSave = () => {
    onSave(person);
    onClose();
  };

  return (
    <Dialog open={open} onClose={onClose} maxWidth="sm" fullWidth>
      <DialogTitle>Editing</DialogTitle>
      <DialogContent>
        <Stack spacing={2}>
          <DialogContentText>Editing only primitive fields</DialogContentText>
          {fields.map((field) => (
            <TextField
              fullWidth
              key={field}
              label={field}
              name={field}
              value={person[field]}
              onChange={handleChange}
              variant="standard"
            />
          ))}
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleSave}>Save</Button>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Dialog>
  );
};

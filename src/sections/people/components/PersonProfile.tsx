import { FC } from 'react';

import { Box, Button, Card, CardContent, CardHeader, Grid, Skeleton, Stack, Typography } from '@mui/material';

import { IPeople } from '../../../redux/people/people.types';

import { PersonProfileField } from './PersonProfileField';

type TPersonProfileProps = {
  data: IPeople;
  onEdit: () => void;
};

export const PersonProfile: FC<TPersonProfileProps> = ({ data, onEdit }) => {
  const fields = Object.keys(data) as (keyof IPeople)[];

  return (
    <Stack
      spacing={2}
      direction={{
        xs: 'column',
        md: 'row',
      }}
      alignItems="flex-start"
    >
      <Card sx={{ p: 3, flex: 1, width: '100%' }}>
        <Typography variant="h6" gutterBottom>
          Profile
        </Typography>
        <Stack spacing={2}>
          <Typography variant="body2">{data.name}</Typography>
          <Button variant="contained" color="primary" fullWidth onClick={onEdit}>
            Edit
          </Button>
        </Stack>
      </Card>

      <Card sx={{ p: 3, flex: 2, width: '100%' }}>
        <Typography variant="h6" gutterBottom>
          Report
        </Typography>
        <Box
          display="grid"
          gridTemplateColumns={{
            xs: 'repeat(1, 1fr)',
            md: 'repeat(12, 1fr)',
          }}
          gap={2}
        >
          {fields.map((field) => (
            <PersonProfileField key={field} name={field} field={data[field]} />
          ))}
        </Box>
      </Card>
    </Stack>
  );
};

export const PersonProfileSkeleton = () => (
    <Stack
      spacing={2}
      direction={{
        xs: 'column',
        md: 'row',
      }}
      alignItems="flex-start"
    >
      <Card sx={{ p: 3, flex: 1, width: '100%' }}>
        <CardHeader title={<Skeleton />} />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            <Skeleton />
            <Skeleton />
          </Typography>
        </CardContent>
      </Card>
      <Card sx={{ p: 3, flex: 2, width: '100%' }}>
        <CardHeader title={<Skeleton />} />
        <CardContent>
          <Grid container spacing={2}>
            {new Array(10).fill(null).map((_, index) => (
              <Grid key={index} item xs={12} md={6}>
                <Skeleton />
              </Grid>
            ))}
          </Grid>
        </CardContent>
      </Card>
    </Stack>
  );

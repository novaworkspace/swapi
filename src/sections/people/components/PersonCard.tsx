import React, { FC } from 'react';

import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import HeightIcon from '@mui/icons-material/Height';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { Button, Card, CardActions, CardContent, CardHeader, Chip, Skeleton, Stack, Typography } from '@mui/material';

import { IPeople } from '../../../redux/people/people.types';

type TPersonCardProps = {
  data: IPeople;
  onClick: (e: React.MouseEvent<any>) => void;
};

export const PersonCard: FC<TPersonCardProps> = ({ data, onClick }) => (
  <Card onClick={onClick}>
    <CardContent>
      <Typography gutterBottom variant="h5" component="div">
        {data.name}
      </Typography>
      <Typography variant="body2" color="text.secondary" />
      <Stack direction="row" spacing={1}>
        <Chip icon={<CardGiftcardIcon />} label={data.birthYear} />
        <Chip icon={<RemoveRedEyeIcon />} label={data.eyeColor} />
        <Chip icon={<HeightIcon />} label={data.height} />
      </Stack>
    </CardContent>
    <CardActions>
      <Button size="small" color="primary" onClick={onClick}>
        Profile
      </Button>
    </CardActions>
  </Card>
);

export const PersonCardSkeleton = () => (
  <Card>
    <CardHeader title={<Skeleton />} />
    <CardContent>
      <Typography variant="body2" color="text.secondary">
        <Skeleton />
        <Skeleton />
      </Typography>
    </CardContent>
  </Card>
);

import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

import { Box, CircularProgress, Pagination, Stack, TextField, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

import { useGetPeopleQuery } from '../../redux/people/people.api';

import { PersonCard, PersonCardSkeleton } from './components/PersonCard';

const GridItem = styled(Box)(({ theme }) => ({
  [theme.breakpoints.down('sm')]: {
    gridColumn: 'span 12',
  },
  [theme.breakpoints.up('md')]: {
    gridColumn: 'span 6',
  },
  [theme.breakpoints.up('lg')]: {
    gridColumn: 'span 4',
  },
}));

const Alert = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.grey[100],
  padding: theme.spacing(5),
  borderRadius: theme.spacing(1),
}));

const content = {
  title: 'Star Wars',
  body: 'Star Wars is an American epic space opera media franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon',
};

export const PeopleList = () => {
  const router = useRouter();

  const [page, setPage] = useState<string>(router.query.page?.toString() || '1');
  const [search, setSearch] = useState<string>(router.query.search?.toString() || '');

  const getPeople = useGetPeopleQuery({ search, page });
  const peopleCount = getPeople.data?.count || 0;
  const pageCount = Math.ceil(peopleCount / 10);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    // update search state
    // and reset page state
    setSearch(e.target.value);
    setPage('1');
    router.push({
      pathname: '/',
      query: {
        search: e.target.value,
        page: '1',
      },
    });
  };

  const handlePagination = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value.toString());
    router.push({
      pathname: '/',
      query: {
        ...router.query,
        page: value.toString(),
      },
    });
  };

  // on change query params update search and page state
  // to support browser history and search bar
  useEffect(() => {
    setSearch(router.query.search?.toString() || '');
    setPage(router.query.page?.toString() || '1');
  }, [router.query.search, router.query.page]);

  return (
    <Stack spacing={4}>
      <Alert>
        <Typography variant="h3">{content.title}</Typography>
        <Typography variant="body2">{content.body}</Typography>
        <Stack direction="row" spacing={2} sx={{ mt: 2 }}>
          <TextField label="Search" value={search} onChange={handleSearch} fullWidth />
        </Stack>
      </Alert>

      <Stack direction="row" spacing={2}>
        <Typography variant="h5">Results</Typography>
        {getPeople.isFetching && <CircularProgress size={20} />}
      </Stack>

      <Box display="grid" gridTemplateColumns="repeat(12, 1fr)" gap={2}>
        {/* Show skeleton */}
        {getPeople.isLoading &&
          new Array(10).fill(0).map((_, index) => (
            <GridItem key={index}>
              <PersonCardSkeleton />
            </GridItem>
          ))}
        {/* Show data */}
        {!getPeople.isLoading &&
          getPeople.data?.results.map((person) => (
            <GridItem key={person.name}>
              <PersonCard data={person} onClick={() => router.push(`/person/${person.id}`)} />
            </GridItem>
          ))}
      </Box>

      <Stack justifyContent="center" alignItems="center" spacing={2}>
        <Pagination count={pageCount} page={Number(page)} onChange={handlePagination} />
      </Stack>
    </Stack>
  );
};

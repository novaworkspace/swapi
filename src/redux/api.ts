import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const apiSlice = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: `https://swapi.dev/api`,
  }),
  keepUnusedDataFor: 30,
  tagTypes: [
    'People',
    'Person',
  ],
  endpoints: () => ({}),
});

import qs from 'qs';

import { apiSlice } from '../api';

import * as mapper from './people.mappers';
import { IApiPeople, IPaginationResponse, IPeople, ISearchRequest } from './people.types';

export const peopleApi = apiSlice.injectEndpoints({
  overrideExisting: false,
  endpoints: (builder) => ({
    getPeople: builder.query<IPaginationResponse<IPeople>, ISearchRequest>({
      query: ({ search, page = '1' }) => ({
        url: `/people?${qs.stringify({ search, page }, { encodeValuesOnly: true })}`,
      }),
      transformResponse: (response: IPaginationResponse<IApiPeople>) => mapper.getPeople(response),
      providesTags: (result, error, { search, page }) => [{ type: 'People', id: `${search}-${page}` }],
    }),
    getPerson: builder.query<IPeople, string>({
      query: (personId) => ({
        url: `/people/${personId}`,
      }),
      transformResponse: (response: IApiPeople) => mapper.getPerson(response),
      providesTags: (result, error, personId) => [{ type: 'Person', id: personId }],
    }),
    // It's emulating a PATCH request
    // There's no actual API endpoint to update a person
    updatePerson: builder.mutation<void, Pick<IPeople, 'id'> & Partial<IPeople>>({
      query: ({ id, ...patch }) => ({
        url: `/people/${id}`,
        method: 'PATCH',
        body: patch,
      }),
      async onQueryStarted({ id, ...patch }, { dispatch, queryFulfilled }) {
        // eslint-disable-next-line no-unused-vars
        const patchResult = dispatch(
          peopleApi.util.updateQueryData('getPerson', id, (draft) => {
            Object.assign(draft, patch);
          })
        );
        try {
          await queryFulfilled;
        } catch {
          // don't revert the changes for emulated mutations
          // patchResult.undo();
          return;
        }
      },
    }),
  }),
});

export const { useGetPeopleQuery, useGetPersonQuery, useUpdatePersonMutation } = peopleApi;

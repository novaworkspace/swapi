export interface ISearchRequest {
  search?: string;
  page?: string;
}

export interface IPaginationResponse<T> {
  count: number;
  next: string | null;
  previous: string | null;
  results: T[];
}

export interface IApiPeople {
  birth_year: string;
  eye_color: string;
  films: string[];
  gender: string;
  hair_color: string;
  height: string;
  homeworld: string;
  mass: string;
  name: string;
  skin_color: string;
  created: Date;
  edited: Date;
  species: string[];
  starships: string[];
  url: string;
  vehicles: string[];
}

export interface IPeople extends Omit<IApiPeople, 'birth_year' | 'eye_color' | 'hair_color' | 'skin_color'> {
  id: string;
  birthYear: string;
  eyeColor: string;
  hairColor: string;
  skinColor: string;
}

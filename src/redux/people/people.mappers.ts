import { IApiPeople, IPaginationResponse, IPeople } from './people.types';

export const getPerson = (person: IApiPeople): IPeople => ({
    ...person,
    id: person.url.split('/')[5],
    birthYear: person.birth_year,
    eyeColor: person.eye_color,
    hairColor: person.hair_color,
    skinColor: person.skin_color,
  });

export const getPeople = (people: IPaginationResponse<IApiPeople>): IPaginationResponse<IPeople> => ({
    ...people,
    results: people.results.map(getPerson),
  });

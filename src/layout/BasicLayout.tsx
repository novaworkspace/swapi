import React from 'react';

import { Box, Container } from '@mui/material';
import { styled } from '@mui/material/styles';

import { SearchAppBar } from '../components/SearchBar';

type BasicLayoutProps = {
  children: React.ReactNode;
};

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

export function BasicLayout({ children }: BasicLayoutProps) {
  return (
    <>
      <SearchAppBar />
      <Offset />
      <Container maxWidth="lg">
        <Box sx={{ py: 3 }}>{children}</Box>
      </Container>
    </>
  );
}

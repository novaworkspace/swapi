import { useRouter } from 'next/router';
import * as React from 'react';

import { BasicLayout } from '../../layout/BasicLayout';
import { PersonEntity } from '../../sections/people';

export default function Home() {
  const router = useRouter();
  const { id } = router.query;
  const personId = id as string;

  return (
    <BasicLayout>
      <PersonEntity personId={personId} />
    </BasicLayout>
  );
}

import * as React from 'react';

import { BasicLayout } from '../layout/BasicLayout';
import { PeopleList } from '../sections/people';

export default function Home() {
  return (
    <BasicLayout>
      <PeopleList />
    </BasicLayout>
  );
}
